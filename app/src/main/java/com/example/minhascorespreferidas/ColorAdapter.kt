package com.example.minhascorespreferidas

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView

class ColorAdapter (var cadastro: Cadastro, var context: Context): BaseAdapter () {
    override fun getCount(): Int {
        return this.cadastro.count()
    }

    override fun getItem(position: Int): Any {
        return this.cadastro.get(position)
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val color = this.cadastro.get(position)
        val linha : View
        if (convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            linha = inflater.inflate(R.layout.activity_color, null)
        }else{
            linha = convertView
        }
        val tvN = linha.findViewById<TextView>(R.id.tvLvName)
        val llHexColor = linha.findViewById<LinearLayout>(R.id.llHexColor)
        tvN.text = color.name
        llHexColor.setBackgroundColor(Color.parseColor(color.hex))
        return linha
    }

}