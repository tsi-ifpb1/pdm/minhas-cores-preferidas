package com.example.minhascorespreferidas

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.lang.IllegalArgumentException
import com.example.minhascorespreferidas.Color as MyColor

class AddColorActivity : AppCompatActivity() {

    private lateinit var etName: EditText
    private lateinit var etHex: EditText
    private lateinit var btSave: Button
    private lateinit var tvPreview: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_color)

        this.etName = findViewById(R.id.etAddName)
        this.etHex = findViewById(R.id.etAddHex)
        this.btSave = findViewById(R.id.btAddSave)
        this.tvPreview = findViewById(R.id.tvAddPreview)

        this.etHex.addTextChangedListener(OnTextChange())

        this.btSave.setOnClickListener(OnClickSave())
    }

    fun updatePreview() {
        if(this.isHexColor()) {
            this.tvPreview.setBackgroundColor(Color.parseColor(this.etHex.text.toString()))
        } else {
            Toast.makeText(this, "Informe um valor hexadecimal válido!", Toast.LENGTH_LONG).show()
        }
    }

    fun isHexLength (): Boolean {
        if (this.etHex.text.toString().length >= 7) {
            return true
        }
        return false
    }

    fun isHexColor (): Boolean {
        if(this.isHexLength()) {
            try     {
                Color.parseColor(this.etHex.text.toString())
            } catch (error : IllegalArgumentException) {
                return false
            }
        } else {
            return false
        }
        return true
    }

    inner class OnClickSave: View.OnClickListener {
        override fun onClick(v: View?) {
            val name = this@AddColorActivity.etName.text.toString()
            val hex = this@AddColorActivity.etHex.text.toString()

            if (name.isNotEmpty() && this@AddColorActivity.isHexColor()) {

                val color = MyColor(name, hex)

                val it = Intent()
                it.putExtra("COLOR", color)

                setResult(Activity.RESULT_OK, it)

                finish()
            } else {
                Toast.makeText(this@AddColorActivity,"Preencha os campos corretamente!", Toast.LENGTH_LONG).show()
            }

        }

    }

    inner class OnTextChange: TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun afterTextChanged(s: Editable?) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (this@AddColorActivity.isHexLength()) {
                this@AddColorActivity.updatePreview()
            }
        }

    }
}
