package com.example.minhascorespreferidas

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var lvColors: ListView
    private lateinit var cadastro: Cadastro
    private lateinit var fabAddColor: FloatingActionButton

    private val COLOR = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.fabAddColor = findViewById(R.id.fabMainAddColor)
        this.lvColors = findViewById(R.id.lvMainColors)

        this.cadastro = Cadastro()

        this.fabAddColor.setOnClickListener(OnClickFAB())

        this.lvColors.adapter = ColorAdapter(this.cadastro, this)
        this.lvColors.setOnItemLongClickListener(LongClickList())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == COLOR) {
            val cor = data?.getParcelableExtra<Color>("COLOR")

            if(cor != null) {
                this.cadastro.add(cor)
                this.updateColorsList()
            }
        }
    }

    fun updateColorsList() {
        (this.lvColors.adapter as ColorAdapter).notifyDataSetChanged()
    }

    inner class OnClickFAB: View.OnClickListener {
        override fun onClick(v: View?) {
            val it = Intent(this@MainActivity, AddColorActivity::class.java)
            startActivityForResult(it, COLOR)
//            this@MainActivity.addColorsList(Color("Preto", "#000000"))
        }
    }

    inner class LongClickList: AdapterView.OnItemLongClickListener {
        override fun onItemLongClick(
            parent: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ): Boolean {
            this@MainActivity.cadastro.get().removeAt(position)
            this@MainActivity.updateColorsList()
            return true
        }
    }
}
