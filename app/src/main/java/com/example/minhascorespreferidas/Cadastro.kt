package com.example.minhascorespreferidas

class Cadastro {
    private lateinit var lista: ArrayList<Color>

    init {
        this.lista = arrayListOf()
    }

    fun add(cor: Color){
        this.lista.add(cor)
    }

    fun get(index: Int) : Color{
        return this.lista.get(index)
    }

    fun get(): ArrayList<Color>{
        return this.lista
    }

    fun count(): Int{
        return this.lista.count()
    }
}